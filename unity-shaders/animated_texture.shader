﻿Shader "Unlit/animated_texture"
{   
    // Outside properties that can be influenced by the user.
	Properties
	{
		_firstTex ("firstTex", 2D) = "defaulttexture" {}
		_secondTex ("secondTex", 2D) = "defaulttexture" {}
		_speed("speed",Float) = 1.0
	}
	
	// Every shader is made out of subshaders.
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};


			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
                // just pass the texture coordinate
                o.uv = v.uv;

				return o;
			}
			
			sampler2D _firstTex;
			sampler2D _secondTex;
			float _speed; 
            
            // The actual fragment shader that does the animation.
			fixed4 frag (v2f i) : SV_Target
			{
				float time = fmod(_Time.y*_speed,1);
				fixed4 col;

				if(time<0.5){
					col = tex2D(_firstTex, i.uv);
				}
				else{
					col = tex2D(_secondTex, i.uv);
				};
				if(col.a < 0.1) discard;
				return col;
			}
			ENDCG
		}
	}
}


