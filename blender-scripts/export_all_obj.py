import bpy
import os

blend_file_path = bpy.data.filepath
directory = os.path.dirname(blend_file_path)

# Get all objects from the current scene.
objects = bpy.data.objects

for object in objects:
    # Skip if it's a Camera or Lamp
    # TODO currently checks name but could check type for Meshes only.
    if object.name in ["Camera", "Lamp"]:
        continue
    
    # Make sure everything is deselected.
    bpy.ops.object.select_all(action='DESELECT')
    # Select the current object.
    object.select = True
    
    # IMPORTANT: Give the object a decent name before exporting it!
    export_name = directory + '/' + object.name + '.fbx'
    
    # Remember the old location and move the object to origin.
    # IMPORTANT: The pivot point of the object should be correctly placed in the middle of the object!
    old_location = object.location[:]
    object.location = (0,0,0)
    
    # Export with embedded textures. (Not sure if this actually works?)
    bpy.ops.export_scene.fbx(filepath=export_name, use_selection=True, embed_textures=True)
    
    object.location = old_location
