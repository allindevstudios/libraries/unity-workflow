﻿using UnityEngine;
//Import UnityEditor only when on editor, in order to be able to build project.
#if UNITY_EDITOR
using UnityEditor;
#endif

// Editor Script that enables gridmapping for Unity3D on prefabs with the tag: "GridTile".
// Tries to mimic the Godot Gridmap as much as possible!
public class GridSnapper : EditorWindow
{
	private Vector3 previousPosition;
	private Quaternion previousRotation;

	private bool doSnap = true;
	private int gridSize = 2; // Unit size of each grid, should nver be changed.
	private int floorLevel = 0; // Moves up in steps of the gridSize.
	private int rotationAngle = 90; // Constrains the rotation angle in steps of 90 deg.

    // Can be called be either using the Edit menu or the CTRL+L shortcut.
	[MenuItem( "Edit/Auto Snap %_l" )]
	static void Init()
	{
		var window = (GridSnapper)EditorWindow.GetWindow( typeof( GridSnapper ) );
		window.maxSize = new Vector2( 200, 100 );
	}
    
    // This function is called when the object becomes enabled and active.
	void OnEnable()
	{
        // Register the GridSnapper to automatically work when dragging and rotating objects.
        // Unregister first if the delegate had previously been added.
		SceneView.onSceneGUIDelegate -= this.OnSceneGUI;
		SceneView.onSceneGUIDelegate += this.OnSceneGUI;
        
        // Not sure why two times?
		//SceneView.onSceneGUIDelegate -= this.OnSceneGUI;
		//SceneView.onSceneGUIDelegate += this.OnSceneGUI;
	}
    
    void OnDestroy() 
    {
        //Unregister if the object is destroyed
        #if UNITY_EDITOR
        SceneView.onSceneGUIDelegate -= this.OnSceneGUI;
        #endif
	}
    
    // Correctly create the GUI and link to local variables.
	public void OnGUI()
	{
		doSnap = EditorGUILayout.Toggle( "Auto Snap", doSnap );
		gridSize = EditorGUILayout.IntField( "Grid Size", gridSize );
		rotationAngle = EditorGUILayout.IntField( "Rotation Angle", rotationAngle );
		floorLevel = EditorGUILayout.IntField( "Floor Level", floorLevel );
	}
	
    #if UNITY_EDITOR
	// Enables the Editor to handle an event in the scene view.
	void OnSceneGUI(SceneView sceneView)
	{
		if (isSnapping())
		{
			Snap();
			previousPosition = Selection.transforms[0].position;
			previousRotation = Selection.transforms[0].rotation;
			// Draw();
		}
	}
	#endif

	private void Snap()
	{
		foreach ( var gameObject in Selection.transforms )
		{
			if (gameObject.tag=="GridTile")
			{
				var position = gameObject.transform.position;
				position.x = Round( position.x, gridSize);
				position.z = Round( position.z, gridSize);
				position.y = floorLevel*gridSize;
				gameObject.transform.position = position;

				Vector3 rotation = gameObject.transform.rotation.eulerAngles;
				rotation.x = -90; //This is a bit weird... why?
				// This is a conflict between Unity3D and Blender as Blender uses Z-axis as up, while Unity3D prefers the y-axis.
				// As a result all exported objects from Blender should be rotated on the x-axis to account for this.
				rotation.y = Round( rotation.y, rotationAngle);
				rotation.z = 0;
				gameObject.transform.rotation = Quaternion.Euler(rotation);
			}
		}
	}

	private float Round( float input, float snapValue )
	{
		return snapValue * Mathf.Round( ( input / snapValue ) );
	}

	private bool isSnapping()
	{
		return (doSnap
		&& !EditorApplication.isPlaying
		&& Selection.transforms.Length > 0
		&& (Selection.transforms[0].position != previousPosition || Selection.transforms[0].rotation != previousRotation));
	}
    
    // Some debugging tool.
	// void Draw()
	// {
	// 	Handles.color = Color.white;
	// 	Handles.DrawLine(Selection.transforms[0].position,Selection.transforms[0].position+10*Vector3.one);
	// }

 }
