//Create a folder (right click in the Assets directory, click Create>New Folder) and name it “Editor” if one doesn’t exist already.
//Place this script in that folder

//This script creates a new menu and a new menu item in the Editor window
// Use the new menu item to create a prefab at the given path. If a prefab already exists it asks if you want to replace it
//Click on a GameObject in your Hierarchy, then go to Examples>Create Prefab to see it in action.

using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.IO;
using System.Linq;

public class CreatePrefabs : EditorWindow
{

    const string rawFolder = "Assets/Raw";
    const string gridTilesFolder = rawFolder + "/GridTiles";
    const string importManuallyFolder = rawFolder + "/ImportManually";

    //Creates a new menu (Custom Importer) with a menu item (Create Prefabs)
    [MenuItem("Custom Importer/Create Prefabs")]
    static void CreatePrefabFromFBX()
    {

        CreateGridTileTag();
        //string sAssetFolderPath = "Assets/RawAssets/GridTiles";
        //string[] aux = sAssetFolderPath.Split(new char[] { '/' });
        //string onlyFolderPath = aux[0] + "/" + aux[1] + "/" + aux[2] + "/";
        //Debug.Log(onlyFolderPath);

        IEnumerable<string> aFilePaths = Directory.GetFiles(rawFolder, "*.*", SearchOption.AllDirectories).Where(s => s.EndsWith(".FBX") || s.EndsWith(".fbx"));

        //string[] aFilePaths = Directory.GetFiles(onlyFolderPath);
 
        foreach (string sFilePath in aFilePaths)
        {
            if (!sFilePath.StartsWith(importManuallyFolder))
            {
                //Debug.Log(sFilePath);

                Object objAsset = AssetDatabase.LoadAssetAtPath(sFilePath, typeof(Object));

                //Set the path identical as in the Raw folder, and name it as the GameObject's name with the .prefab format
                string[] aux = sFilePath.Split(new char[] { '/' });
                //Debug.Log(aux.Length);
                string localPath = "";
                foreach (int idx in Enumerable.Range(2, aux.Length-3))
                {
                    //Debug.Log(aux[idx]);
                    localPath = localPath + aux[idx] + "/";
                }
                localPath = "Assets/Prefabs/" + localPath + objAsset.name + ".prefab";

                GameObject gameObject = (GameObject)Instantiate(objAsset);

                //Add tag for using the prefab in the GridSnapper.
                if (sFilePath.StartsWith(gridTilesFolder))
                {
                    gameObject.tag = "GridTile";
                }

                CreateNewPrefab(gameObject, localPath);
                DestroyImmediate(gameObject);
            }
        }
    }

    static void CreateGridTileTag()
    {
        // Open tag manager
        SerializedObject tagManager = new SerializedObject(AssetDatabase.LoadAllAssetsAtPath("ProjectSettings/TagManager.asset")[0]);
        SerializedProperty tagsProp = tagManager.FindProperty("tags");

        // Adding a Tag
        string s = "GridTile";

        // First check if it is not already present
        bool found = false;
        for (int i = 0; i < tagsProp.arraySize; i++)
        {
            SerializedProperty t = tagsProp.GetArrayElementAtIndex(i);
            if (t.stringValue.Equals(s)) { found = true; break; }
        }

        // if not found, add it
        if (!found)
        {
            tagsProp.InsertArrayElementAtIndex(0);
            SerializedProperty n = tagsProp.GetArrayElementAtIndex(0);
            n.stringValue = s;
            tagManager.ApplyModifiedPropertiesWithoutUndo();
        }
    }
    
    static void CreateNewPrefab(GameObject gameObject, string localPath)
    {
        //Create a new prefab at the path given
        Object prefab = PrefabUtility.CreatePrefab(localPath, gameObject);
        PrefabUtility.ReplacePrefab(gameObject, prefab, ReplacePrefabOptions.ConnectToPrefab);
    }
}

//Loop through every GameObject in the array above
// foreach (GameObject gameObject in objectArray)
// {
//     //Set the path as within the Assets folder, and name it as the GameObject's name with the .prefab format
//     string localPath = "Assets/" + gameObject.name + ".prefab";

//     //Check if the Prefab and/or name already exists at the path
//     if (AssetDatabase.LoadAssetAtPath(localPath, typeof(GameObject)))
//     {
//         //Create dialog to ask if User is sure they want to overwrite existing prefab
//         if (EditorUtility.DisplayDialog("Are you sure?",
//                 "The prefab already exists. Do you want to overwrite it?",
//                 "Yes",
//                 "No"))
//         //If the user presses the yes button, create the Prefab
//         {
//             CreateNew(gameObject, localPath);
//         }
//     }
//     //If the name doesn't exist, create the new Prefab
//     else
//     {
//         Debug.Log(gameObject.name + " is not a prefab, will convert");
//         CreateNew(gameObject, localPath);
//     }
// }
