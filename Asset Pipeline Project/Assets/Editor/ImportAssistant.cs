using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
    
public class ImportAssistant : AssetPostprocessor {
    
    void OnPreprocessAsset()
    {
        ModelImporter importer = assetImporter as ModelImporter;
        if (importer != null)
            {
                // Force the importer to search for the texture.
                importer.materialLocation = ModelImporterMaterialLocation.External;
            }
        //Debug.Log("zwolo");
    }
}
